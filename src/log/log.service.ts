import { Injectable } from '@nestjs/common';
import { CreateLogDto } from './dto/create-log.dto';
import { UpdateLogDto } from './dto/update-log.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Log } from './entities/log.entity';
import { Repository } from 'typeorm';
import { Juristic } from 'src/juristics/entities/juristic.entity';
import { Resident } from 'src/residents/entities/resident.entity';

@Injectable()
export class LogService {
  constructor(
    @InjectRepository(Log)
    private logsRepository: Repository<Log>,
    @InjectRepository(Juristic)
    private juristicsRepository: Repository<Juristic>,
    @InjectRepository(Resident)
    private residentsRepository: Repository<Resident>,
  ) {}

  async create(createLogDto: CreateLogDto) {
    const log = new Log();
    log.activity = createLogDto.activity;
    if (createLogDto.juristicId) {
      const juristic = await this.juristicsRepository.findOneBy({
        id: createLogDto.juristicId,
      });
      log.juristic = juristic;
    }
    if (createLogDto.residentId) {
      const resident = await this.residentsRepository.findOneBy({
        id: createLogDto.residentId,
      });
      log.resident = resident;
    }
    await this.logsRepository.save(log);
    return log;
  }

  findAll() {
    return this.logsRepository.find({
      relations: ['juristic', 'resident'],
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} log`;
  }

  update(id: number, updateLogDto: UpdateLogDto) {
    return `This action updates a #${id} log`;
  }

  remove(id: number) {
    return `This action removes a #${id} log`;
  }
}
