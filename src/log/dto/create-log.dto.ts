import { IsNotEmpty } from 'class-validator';

export class CreateLogDto {
  @IsNotEmpty()
  activity: string;

  juristicId: number;

  residentId: number;
}
