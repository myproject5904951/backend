import { Juristic } from 'src/juristics/entities/juristic.entity';
import { Resident } from 'src/residents/entities/resident.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Log {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  date: Date;

  @Column()
  activity: string;

  @ManyToOne(() => Juristic, (juristic) => juristic.log)
  juristic: Juristic;

  @ManyToOne(() => Resident, (resident) => resident.log)
  resident: Resident;
}
