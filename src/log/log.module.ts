import { Module } from '@nestjs/common';
import { LogService } from './log.service';
import { LogController } from './log.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Log } from './entities/log.entity';
import { Juristic } from 'src/juristics/entities/juristic.entity';
import { Resident } from 'src/residents/entities/resident.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Log, Juristic, Resident])],
  controllers: [LogController],
  providers: [LogService],
})
export class LogModule {}
