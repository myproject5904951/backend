import { IsNotEmpty } from 'class-validator';
import { Juristic } from 'src/juristics/entities/juristic.entity';
import { Resident } from 'src/residents/entities/resident.entity';

export class CreateReceiptDto {
  @IsNotEmpty()
  sub_total: number;

  package: string;

  @IsNotEmpty()
  discount: number;

  @IsNotEmpty()
  total: number;

  @IsNotEmpty()
  paid: number;

  @IsNotEmpty()
  change: number;

  @IsNotEmpty()
  payment_method: string;

  @IsNotEmpty()
  billImg: string;

  @IsNotEmpty()
  resident: Resident;

  juristic: Juristic;
}
