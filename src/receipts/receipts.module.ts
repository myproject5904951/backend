import { Module } from '@nestjs/common';
import { ReceiptsService } from './receipts.service';
import { ReceiptsController } from './receipts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Invoice } from 'src/invoices/entities/invoice.entity';
import { Juristic } from 'src/juristics/entities/juristic.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Receipt, Invoice, Juristic])],
  controllers: [ReceiptsController],
  providers: [ReceiptsService],
})
export class ReceiptsModule {}
