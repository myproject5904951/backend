import { Invoice } from 'src/invoices/entities/invoice.entity';
import { Juristic } from 'src/juristics/entities/juristic.entity';
import { Resident } from 'src/residents/entities/resident.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('decimal', {
    precision: 10,
    scale: 2,
  })
  sub_total: number;

  @Column()
  package: string;

  // @Column({ type: 'decimal', nullable: true })
  @Column('decimal', {
    precision: 10,
    scale: 2,
  })
  fines: number;

  @Column('decimal', {
    precision: 10,
    scale: 2,
  })
  discount: number;

  @Column('decimal', {
    precision: 10,
    scale: 2,
  })
  total: number;

  @Column('decimal', {
    precision: 10,
    scale: 2,
  })
  paid: number;

  @Column('decimal', {
    precision: 10,
    scale: 2,
  })
  change: number;

  @CreateDateColumn()
  date: Date;

  @Column()
  payment_method: string;

  @Column({ type: 'longtext' })
  billImg: string;

  @OneToMany(() => Invoice, (invoice) => invoice.receipt)
  invoice: Invoice[];

  @ManyToOne(() => Juristic, (juristic) => juristic.receipt)
  juristic: Juristic;

  @ManyToOne(() => Resident, (resident) => resident.receipt)
  resident: Resident;

  @DeleteDateColumn()
  deletedDate: Date;
}
