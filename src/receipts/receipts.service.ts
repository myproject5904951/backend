import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Repository } from 'typeorm';
import { Invoice } from 'src/invoices/entities/invoice.entity';
import { Juristic } from 'src/juristics/entities/juristic.entity';

@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt)
    private receiptsRepository: Repository<Receipt>,
    @InjectRepository(Invoice)
    private invoicesRepository: Repository<Invoice>,
    @InjectRepository(Juristic)
    private juristicsRepository: Repository<Juristic>,
  ) {}

  async create(createReceiptDto: CreateReceiptDto) {
    const receipt = await this.receiptsRepository.save(createReceiptDto);
    return receipt;
  }

  findAll() {
    return this.receiptsRepository.find({
      relations: ['invoice', 'resident', 'juristic'],
    });
  }

  async findOne(id: number) {
    const receipt = await this.receiptsRepository.findOne({
      where: { id },
      relations: ['invoice', 'resident', 'juristic'],
    });
    if (!receipt) {
      throw new NotFoundException();
    }
    return receipt;
  }

  async update(id: number, updateReceiptDto: UpdateReceiptDto) {
    const receipt = await this.receiptsRepository.findOneBy({ id: id });
    if (!receipt) {
      throw new NotFoundException();
    }
    const updatedReceipt = { ...receipt, ...updateReceiptDto };
    const updReceipt = await this.receiptsRepository.save(updatedReceipt);
    return updReceipt;
  }

  async remove(id: number) {
    const receipt = await this.receiptsRepository.findOne({
      where: { id },
      relations: ['invoice'],
    });
    for (const inv of receipt.invoice) {
      const i = await this.invoicesRepository.findOne({
        where: { id: inv.id },
        relations: ['receipt'],
      });
      i.receipt = null;
      await this.invoicesRepository.update(i.id, i);
    }
    if (!receipt) {
      throw new NotFoundException();
    }
    receipt.invoice = [];
    return await this.receiptsRepository.softRemove(receipt);
  }
}
