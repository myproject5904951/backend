import { Module } from '@nestjs/common';
import { ResidentsService } from './residents.service';
import { ResidentsController } from './residents.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Resident } from './entities/resident.entity';
import { User } from 'src/users/entities/user.entity';
import { House } from 'src/houses/entities/house.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Resident, User, House])],
  controllers: [ResidentsController],
  providers: [ResidentsService],
})
export class ResidentsModule {}
