import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateResidentDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  phone: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  // @IsNotEmpty()
  dateOfStay: Date;

  // @IsNotEmpty()
  house_number: string;
}
