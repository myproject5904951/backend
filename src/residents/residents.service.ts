import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateResidentDto } from './dto/create-resident.dto';
import { UpdateResidentDto } from './dto/update-resident.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Resident } from './entities/resident.entity';
import { User } from 'src/users/entities/user.entity';
import * as bcrypt from 'bcrypt';
import { House } from 'src/houses/entities/house.entity';

@Injectable()
export class ResidentsService {
  constructor(
    @InjectRepository(Resident)
    private residentsRepository: Repository<Resident>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(House)
    private housesRepository: Repository<House>,
  ) {}

  async create(createResidentDto: CreateResidentDto) {
    const user = new User();
    const resident = await this.residentsRepository.save(createResidentDto);
    user.username = createResidentDto.email;
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(createResidentDto.phone, salt);
    user.password = hash;
    user.role = 3;
    user.resident = resident;
    await this.usersRepository.save(user);
    for (const i of resident.house_number) {
      const house = await this.housesRepository.findOne({
        where: { house_number: resident.house_number },
      });
      house.resident = resident;
      const newH = await this.housesRepository.save(house);
      // console.log(newH);
    }
    return this.residentsRepository.findOne({
      where: { id: resident.id },
      relations: ['user', 'house', 'receipt'],
    });
  }

  findAll() {
    return this.residentsRepository.find({
      relations: ['user', 'house', 'receipt'],
    });
  }

  async findOne(id: number) {
    const resident = await this.residentsRepository.findOne({
      where: { id: id },
      relations: ['user', 'house', 'receipt'],
    });
    if (!resident) {
      throw new NotFoundException();
    }
    return resident;
  }

  async findHouseByResidentID(id: number) {
    const resident = await this.residentsRepository.findOne({
      where: { id: id },
      relations: ['house'],
    });
    if (!resident) {
      throw new NotFoundException();
    }
    return resident;
  }

  async update(id: number, updateResidentDto: UpdateResidentDto) {
    const oldResident = await this.residentsRepository.findOne({
      where: { id: id },
      relations: ['user', 'house'],
    });

    if (!!updateResidentDto.name) {
      oldResident.name = updateResidentDto.name;
    }
    if (!!updateResidentDto.phone) {
      oldResident.phone = updateResidentDto.phone;
    }
    if (!!updateResidentDto.email) {
      oldResident.email = updateResidentDto.email;
    }
    const res = await this.residentsRepository.save(oldResident);
    // add new house
    if (!!updateResidentDto.house_number) {
      const house = await this.housesRepository.findOne({
        where: { house_number: updateResidentDto.house_number },
        relations: ['resident'],
      });
      house.resident = res;
      await this.housesRepository.save(house);
    }
    const resident = await this.residentsRepository.findOne({
      where: { id: id },
      relations: ['house'],
    });
    return resident;
  }

  async remove(id: number) {
    const resident = await this.residentsRepository.findOne({
      where: { id: id },
      relations: ['user', 'house'],
    });
    if (!resident) {
      throw new NotFoundException();
    }
    await this.usersRepository.remove(resident.user);
    for (const i in resident.house) {
      const id = resident.house[i].id;
      const house = await this.housesRepository.findOneBy({
        id: id,
      });
      house.resident = null;
      await this.housesRepository.save(house);
    }
    return this.residentsRepository.remove(resident);
  }
}
