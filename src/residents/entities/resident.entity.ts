import { House } from 'src/houses/entities/house.entity';
import { Invoice } from 'src/invoices/entities/invoice.entity';
import { Log } from 'src/log/entities/log.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Resident {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ length: 10 })
  phone: string;

  @Column()
  email: string;

  @CreateDateColumn()
  dateOfStay: Date;

  // @Column()
  // dateOfMoveOut: Date;

  @OneToOne(() => User, (user) => user.resident)
  user: User;

  @OneToMany(() => House, (house) => house.resident)
  house: House[];

  @OneToMany(() => Invoice, (invoice) => invoice.resident)
  invoice: Invoice[];

  @OneToMany(() => Receipt, (receipt) => receipt.resident)
  receipt: Receipt[];

  @OneToMany(() => Log, (log) => log.resident)
  log: Log;
}
