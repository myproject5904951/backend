import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { DataSource } from 'typeorm';
import { AuthModule } from './auth/auth.module';
import { JuristicsModule } from './juristics/juristics.module';
import { ResidentsModule } from './residents/residents.module';
import { Resident } from './residents/entities/resident.entity';
import { Juristic } from './juristics/entities/juristic.entity';
import { HousesModule } from './houses/houses.module';
import { House } from './houses/entities/house.entity';
import { InvoicesModule } from './invoices/invoices.module';
import { Invoice } from './invoices/entities/invoice.entity';
import { ReceiptsModule } from './receipts/receipts.module';
import { Receipt } from './receipts/entities/receipt.entity';
import { CostsModule } from './costs/costs.module';
import { Cost } from './costs/entities/cost.entity';
import { CostReceiptsModule } from './cost_receipts/cost_receipts.module';
import { CostReceipt } from './cost_receipts/entities/cost_receipt.entity';
import { LogModule } from './log/log.module';
import { Log } from './log/entities/log.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'my-house',
      password: 'password',
      database: 'my-house',
      synchronize: true,
      entities: [
        User,
        Resident,
        Juristic,
        House,
        Invoice,
        Receipt,
        Cost,
        CostReceipt,
        Log,
      ],
    }),
    UsersModule,
    AuthModule,
    JuristicsModule,
    ResidentsModule,
    HousesModule,
    InvoicesModule,
    ReceiptsModule,
    CostsModule,
    CostReceiptsModule,
    LogModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
