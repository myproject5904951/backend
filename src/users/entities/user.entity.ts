import { Juristic } from 'src/juristics/entities/juristic.entity';
import { Resident } from 'src/residents/entities/resident.entity';
import {
  Column,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column()
  role: number;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToOne(() => Juristic, (juristic) => juristic.user)
  @JoinColumn()
  juristic: Juristic;

  @OneToOne(() => Resident, (resident) => resident.user)
  @JoinColumn()
  resident: Resident;
}
