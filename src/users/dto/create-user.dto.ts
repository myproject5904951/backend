import { IsNotEmpty, Matches } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  username: string;

  @IsNotEmpty()
  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@_$!%*?&#]{8,}$/,
  )
  password: string;

  @IsNotEmpty()
  role: number;

  juristicId: number;

  residentId: number;
}
