import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateHouseDto } from './dto/create-house.dto';
import { UpdateHouseDto } from './dto/update-house.dto';
import { House } from './entities/house.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Resident } from 'src/residents/entities/resident.entity';

@Injectable()
export class HousesService {
  constructor(
    @InjectRepository(House)
    private housesRepository: Repository<House>,
    @InjectRepository(House)
    private residentRepository: Repository<Resident>,
  ) {}

  create(createHouseDto: CreateHouseDto) {
    return this.housesRepository.save(createHouseDto);
  }

  findAll() {
    return this.housesRepository.find({ relations: ['resident'] });
  }

  async findOne(id: number) {
    const house = await this.housesRepository.findOne({
      where: { id: id },
      relations: ['resident'],
    });
    if (!house) {
      throw new NotFoundException();
    }
    return house;
  }

  async update(id: number, updateHouseDto: UpdateHouseDto) {
    const house = await this.housesRepository.findOneBy({ id: id });
    if (!house) {
      throw new NotFoundException();
    }
    const updatedHouse = { ...house, ...updateHouseDto };
    return this.housesRepository.save(updatedHouse);
  }

  async remove(id: number) {
    const house = await this.housesRepository.findOneBy({ id: id });
    if (!house) {
      throw new NotFoundException();
    }
    return this.housesRepository.remove(house);
  }
}
