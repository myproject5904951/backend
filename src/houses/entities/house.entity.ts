import { Resident } from 'src/residents/entities/resident.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class House {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  house_number: string;

  @Column({ type: 'decimal' })
  area: number;

  // @Column()
  // type: string;

  @ManyToOne(() => Resident, (resident) => resident.house)
  resident: Resident;
}
