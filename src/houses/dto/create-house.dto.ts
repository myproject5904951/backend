import { IsDecimal, IsNotEmpty } from 'class-validator';
import { Resident } from 'src/residents/entities/resident.entity';

export class CreateHouseDto {
  @IsNotEmpty()
  house_number: string;

  @IsNotEmpty()
  @IsDecimal()
  area: number;

  // type: string;

  residentId: number;

  resident: Resident;
}
