import { Module } from '@nestjs/common';
import { HousesService } from './houses.service';
import { HousesController } from './houses.controller';
import { House } from './entities/house.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Resident } from 'src/residents/entities/resident.entity';

@Module({
  imports: [TypeOrmModule.forFeature([House, Resident])],
  controllers: [HousesController],
  providers: [HousesService],
})
export class HousesModule {}
