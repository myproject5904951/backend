import { Test, TestingModule } from '@nestjs/testing';
import { CostReceiptsService } from './cost_receipts.service';

describe('CostReceiptsService', () => {
  let service: CostReceiptsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CostReceiptsService],
    }).compile();

    service = module.get<CostReceiptsService>(CostReceiptsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
