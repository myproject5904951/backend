import { PartialType } from '@nestjs/mapped-types';
import { CreateCostReceiptDto } from './create-cost_receipt.dto';

export class UpdateCostReceiptDto extends PartialType(CreateCostReceiptDto) {}
