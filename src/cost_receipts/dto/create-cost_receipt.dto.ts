import { IsNotEmpty } from 'class-validator';
import { Cost } from 'src/costs/entities/cost.entity';

export class CreateCostReceiptDto {
  @IsNotEmpty()
  total: number;

  payDate: Date;

  @IsNotEmpty()
  billImg: string;

  note: string;

  // @IsNotEmpty()
  costId: number;

  @IsNotEmpty()
  cost: Cost;

  @IsNotEmpty()
  jurisyicId: number;
}
