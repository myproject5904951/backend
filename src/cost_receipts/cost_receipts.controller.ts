import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { CostReceiptsService } from './cost_receipts.service';
import { CreateCostReceiptDto } from './dto/create-cost_receipt.dto';
import { UpdateCostReceiptDto } from './dto/update-cost_receipt.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('cost-receipts')
export class CostReceiptsController {
  constructor(private readonly costReceiptsService: CostReceiptsService) {}

  @Post()
  create(@Body() createCostReceiptDto: CreateCostReceiptDto) {
    return this.costReceiptsService.create(createCostReceiptDto);
  }

  @Get()
  findAll() {
    return this.costReceiptsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.costReceiptsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCostReceiptDto: UpdateCostReceiptDto,
  ) {
    return this.costReceiptsService.update(+id, updateCostReceiptDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.costReceiptsService.remove(+id);
  }
}
