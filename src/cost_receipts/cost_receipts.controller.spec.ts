import { Test, TestingModule } from '@nestjs/testing';
import { CostReceiptsController } from './cost_receipts.controller';
import { CostReceiptsService } from './cost_receipts.service';

describe('CostReceiptsController', () => {
  let controller: CostReceiptsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CostReceiptsController],
      providers: [CostReceiptsService],
    }).compile();

    controller = module.get<CostReceiptsController>(CostReceiptsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
