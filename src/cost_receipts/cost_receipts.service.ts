import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCostReceiptDto } from './dto/create-cost_receipt.dto';
import { UpdateCostReceiptDto } from './dto/update-cost_receipt.dto';
import { CostReceipt } from './entities/cost_receipt.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cost } from 'src/costs/entities/cost.entity';
import { Juristic } from 'src/juristics/entities/juristic.entity';

@Injectable()
export class CostReceiptsService {
  constructor(
    @InjectRepository(CostReceipt)
    private costReceiptsRepository: Repository<CostReceipt>,
    @InjectRepository(Cost)
    private costsRepository: Repository<Cost>,
    @InjectRepository(Juristic)
    private juristicsRepository: Repository<Juristic>,
  ) {}
  async create(createCostReceiptDto: CreateCostReceiptDto) {
    const costReceipt = new CostReceipt();
    // const cost = await this.costsRepository.findOne({
    //   where: { id: createCostReceiptDto.costId },
    // });
    const juristic = await this.juristicsRepository.findOne({
      where: { id: createCostReceiptDto.jurisyicId },
    });
    costReceipt.total = createCostReceiptDto.total;
    costReceipt.billImg = createCostReceiptDto.billImg;
    if (costReceipt.note !== null) {
      costReceipt.note = createCostReceiptDto.note;
    }
    costReceipt.cost = createCostReceiptDto.cost;
    costReceipt.juristic = juristic;
    return this.costReceiptsRepository.save(costReceipt);
  }

  findAll() {
    return this.costReceiptsRepository.find({
      relations: ['cost', 'juristic'],
    });
  }

  async findOne(id: number) {
    const costReceipt = await this.costReceiptsRepository.findOne({
      where: { id: id },
      relations: ['cost', 'juristic'],
    });
    if (!costReceipt) {
      throw new NotFoundException();
    }
    return costReceipt;
  }

  async update(id: number, updateCostReceiptDto: UpdateCostReceiptDto) {
    await this.costReceiptsRepository.update(id, updateCostReceiptDto);
    return this.costReceiptsRepository.findOneBy({ id });
  }

  async remove(id: number) {
    const inv = await this.costReceiptsRepository.findOneBy({ id: id });
    if (!inv) {
      throw new NotFoundException();
    }
    return this.costReceiptsRepository.softRemove(inv);
  }
}
