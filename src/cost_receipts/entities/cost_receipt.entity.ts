import { Cost } from 'src/costs/entities/cost.entity';
import { Juristic } from 'src/juristics/entities/juristic.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class CostReceipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('decimal', {
    precision: 10,
    scale: 2,
  })
  total: number;

  @CreateDateColumn()
  pay_date: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @Column({ type: 'longtext' })
  billImg: string;

  @Column({ nullable: true })
  note: string;

  @ManyToOne(() => Cost, (cost) => cost.costReceipt)
  cost: Cost;

  @ManyToOne(() => Juristic, (juristic) => juristic.costReceipt)
  juristic: Juristic;
}
