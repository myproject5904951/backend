import { Module } from '@nestjs/common';
import { CostReceiptsService } from './cost_receipts.service';
import { CostReceiptsController } from './cost_receipts.controller';
import { CostReceipt } from './entities/cost_receipt.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cost } from 'src/costs/entities/cost.entity';
import { Juristic } from 'src/juristics/entities/juristic.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CostReceipt, Cost, Juristic])],
  controllers: [CostReceiptsController],
  providers: [CostReceiptsService],
})
export class CostReceiptsModule {}
