import { PartialType } from '@nestjs/mapped-types';
import { CreateJuristicDto } from './create-juristic.dto';

export class UpdateJuristicDto extends PartialType(CreateJuristicDto) {}
