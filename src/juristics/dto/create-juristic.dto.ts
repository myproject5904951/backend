import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateJuristicDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  role: number;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  address: string;

  @IsNotEmpty()
  phone: string;
}
