import { Test, TestingModule } from '@nestjs/testing';
import { JuristicsController } from './juristics.controller';
import { JuristicsService } from './juristics.service';

describe('JuristicsController', () => {
  let controller: JuristicsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [JuristicsController],
      providers: [JuristicsService],
    }).compile();

    controller = module.get<JuristicsController>(JuristicsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
