import { Module } from '@nestjs/common';
import { JuristicsService } from './juristics.service';
import { JuristicsController } from './juristics.controller';
import { Juristic } from './entities/juristic.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Juristic, User])],
  controllers: [JuristicsController],
  providers: [JuristicsService],
})
export class JuristicsModule {}
