import { Test, TestingModule } from '@nestjs/testing';
import { JuristicsService } from './juristics.service';

describe('JuristicsService', () => {
  let service: JuristicsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [JuristicsService],
    }).compile();

    service = module.get<JuristicsService>(JuristicsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
