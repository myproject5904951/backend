import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { JuristicsService } from './juristics.service';
import { CreateJuristicDto } from './dto/create-juristic.dto';
import { UpdateJuristicDto } from './dto/update-juristic.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('juristics')
export class JuristicsController {
  constructor(private readonly juristicsService: JuristicsService) {}

  @Post()
  create(@Body() createJuristicDto: CreateJuristicDto) {
    return this.juristicsService.create(createJuristicDto);
  }

  @Get()
  findAll() {
    return this.juristicsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.juristicsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateJuristicDto: UpdateJuristicDto,
  ) {
    return this.juristicsService.update(+id, updateJuristicDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.juristicsService.remove(+id);
  }
}
