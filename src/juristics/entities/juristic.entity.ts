import { CostReceipt } from 'src/cost_receipts/entities/cost_receipt.entity';
import { Log } from 'src/log/entities/log.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  DeleteDateColumn,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Juristic {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  address: string;

  @Column({ length: 10 })
  phone: string;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToOne(() => User, (user) => user.juristic)
  user: User;

  @OneToMany(() => Receipt, (receipt) => receipt.juristic)
  receipt: Receipt;

  @OneToMany(() => CostReceipt, (costReceipt) => costReceipt.juristic)
  costReceipt: CostReceipt;

  @OneToMany(() => Log, (log) => log.juristic)
  log: Log;
}
