import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateJuristicDto } from './dto/create-juristic.dto';
import { UpdateJuristicDto } from './dto/update-juristic.dto';
import { Juristic } from './entities/juristic.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class JuristicsService {
  constructor(
    @InjectRepository(Juristic)
    private juristicsRepository: Repository<Juristic>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(createJuristicDto: CreateJuristicDto) {
    const user = new User();
    const juristic = await this.juristicsRepository.save(createJuristicDto);
    user.username = createJuristicDto.email;
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(createJuristicDto.phone, salt);
    user.password = hash;
    user.role = createJuristicDto.role;
    user.juristic = juristic;
    await this.usersRepository.save(user);
    return this.usersRepository.findOne({
      where: { id: user.id },
      relations: ['juristic'],
    });
  }

  findAll() {
    return this.juristicsRepository.find();
  }

  async findOne(id: number) {
    const juristic = await this.juristicsRepository.findOne({
      where: { id: id },
      relations: ['user'],
    });
    if (!juristic) {
      throw new NotFoundException();
    }
    return juristic;
  }

  async update(id: number, updateJuristicDto: UpdateJuristicDto) {
    await this.juristicsRepository.update(id, updateJuristicDto);
    return this.juristicsRepository.findOneBy({ id });
  }

  async remove(id: number) {
    const juristic = await this.juristicsRepository.findOne({
      where: { id: id },
      relations: ['user'],
    });
    if (!juristic) {
      throw new NotFoundException();
    }
    await this.usersRepository.softRemove(juristic.user);
    return this.juristicsRepository.softRemove(juristic);
  }
}
