import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Resident } from 'src/residents/entities/resident.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Invoice {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  house_number: string;

  @Column('decimal', {
    precision: 10,
    scale: 2,
  })
  amount: number;

  @Column()
  start_date: Date;

  @Column()
  last_date: Date;

  @ManyToOne(() => Resident, (resident) => resident.invoice)
  resident: Resident;

  @ManyToOne(() => Receipt, (receipt) => receipt.invoice)
  receipt: Receipt;
}
