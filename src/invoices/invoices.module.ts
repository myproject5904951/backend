import { Module } from '@nestjs/common';
import { InvoicesService } from './invoices.service';
import { InvoicesController } from './invoices.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Invoice } from './entities/invoice.entity';
import { Resident } from 'src/residents/entities/resident.entity';
import { House } from 'src/houses/entities/house.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Invoice, Resident, House])],
  controllers: [InvoicesController],
  providers: [InvoicesService],
})
export class InvoicesModule {}
