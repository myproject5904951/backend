import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateInvoiceDto } from './dto/create-invoice.dto';
import { UpdateInvoiceDto } from './dto/update-invoice.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Invoice } from './entities/invoice.entity';
import { Repository } from 'typeorm';
import { Resident } from 'src/residents/entities/resident.entity';
import { House } from 'src/houses/entities/house.entity';

@Injectable()
export class InvoicesService {
  constructor(
    @InjectRepository(Invoice)
    private invoicesRepository: Repository<Invoice>,
    @InjectRepository(Resident)
    private residentsRepository: Repository<Resident>,
    @InjectRepository(House)
    private HousesRepository: Repository<House>,
  ) {}

  async create(createInvoiceDto: CreateInvoiceDto) {
    const house = await this.HousesRepository.find({
      relations: ['resident'],
    });
    for (const h of house) {
      if (h.resident !== null) {
        for (let i = 0; i < 12; i++) {
          const sd = new Date();
          const ld = new Date();
          const inv = new Invoice();
          sd.setMonth(i);
          ld.setMonth(i);
          ld.setDate(6);
          // if (i > 12) {
          //   sd.setFullYear(createInvoiceDto.year + 1);
          //   ld.setFullYear(createInvoiceDto.year + 1);
          // } else {
          sd.setFullYear(createInvoiceDto.year);
          ld.setFullYear(createInvoiceDto.year);
          // }
          inv.house_number = h.house_number;
          inv.amount = 1450;
          inv.start_date = sd;
          inv.last_date = ld;
          inv.resident = h.resident;
          await this.invoicesRepository.save(inv);
        }
      }
    }
    return createInvoiceDto;
  }

  async findAll() {
    const inv = await this.invoicesRepository.find({
      relations: ['resident', 'receipt'],
    });
    return inv;
  }

  async findOne(id: number) {
    const inv = await this.invoicesRepository.findOne({
      where: { id: id },
      relations: ['resident', 'receipt'],
    });
    inv.resident = await this.residentsRepository.findOne({
      where: { id: inv.resident.id },
      relations: ['house'],
    });
    if (!inv) {
      throw new NotFoundException();
    }
    return inv;
  }

  async update(id: number, updateInvoiceDto: UpdateInvoiceDto) {
    const inv = await this.invoicesRepository.findOneBy({ id: id });
    if (!inv) {
      throw new NotFoundException();
    }
    const updatedInv = { ...inv, ...updateInvoiceDto };
    return this.invoicesRepository.save(updatedInv);
  }

  async remove(id: number) {
    const inv = await this.invoicesRepository.findOneBy({ id: id });
    if (!inv) {
      throw new NotFoundException();
    }
    return this.invoicesRepository.remove(inv);
  }
}
