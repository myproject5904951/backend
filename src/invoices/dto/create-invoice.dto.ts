import { IsDecimal, IsNotEmpty } from 'class-validator';
import { Receipt } from 'src/receipts/entities/receipt.entity';

export class CreateInvoiceDto {
  @IsNotEmpty()
  house_number: string;

  @IsNotEmpty()
  @IsDecimal()
  amount: number;

  start_date: Date;

  last_date: Date;

  @IsNotEmpty()
  residentId: number;

  receipt: Receipt;

  @IsNotEmpty()
  year: number;
}
