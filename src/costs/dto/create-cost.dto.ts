import { IsNotEmpty } from 'class-validator';

export class CreateCostDto {
  @IsNotEmpty()
  type: string;
}
