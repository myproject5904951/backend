import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCostDto } from './dto/create-cost.dto';
import { UpdateCostDto } from './dto/update-cost.dto';
import { Cost } from './entities/cost.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CostsService {
  constructor(
    @InjectRepository(Cost)
    private costsRepository: Repository<Cost>,
  ) {}

  create(createCostDto: CreateCostDto) {
    return this.costsRepository.save(createCostDto);
  }

  findAll() {
    return this.costsRepository.find();
  }

  async findOne(type: string) {
    const cost = await this.costsRepository.findOne({
      where: { type: type },
    });
    if (!cost) {
      throw new NotFoundException();
    }
    return cost;
  }

  async update(id: number, updateCostDto: UpdateCostDto) {
    const cost = await this.costsRepository.findOneBy({ id: id });
    if (!cost) {
      throw new NotFoundException();
    }
    const updatedCost = { ...cost, ...updateCostDto };
    return this.costsRepository.save(updatedCost);
  }

  async remove(id: number) {
    const cost = await this.costsRepository.findOneBy({ id: id });
    if (!cost) {
      throw new NotFoundException();
    }
    return this.costsRepository.remove(cost);
  }
}
