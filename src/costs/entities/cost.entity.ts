import { CostReceipt } from 'src/cost_receipts/entities/cost_receipt.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Cost {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: string;

  @OneToMany(() => CostReceipt, (costReceipt) => costReceipt.cost)
  costReceipt: CostReceipt;
}
